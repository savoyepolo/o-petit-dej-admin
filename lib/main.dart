import 'package:flutter/material.dart';

void main() {
  runApp(App());
}

class Users extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              width: MediaQuery.of(context).size.width * 0.25,
              decoration: BoxDecoration(
                  border: Border(
                right: BorderSide(
                  color: Color(0xFFE94444),
                  width: 3.0,
                ),
              )),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  SizedBox(
                    height: 12,
                  ),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                            width: 250,
                            child: TextField(
                              decoration: InputDecoration(
                                  focusedBorder: OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Color(0xFFE94444)),
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(10))),
                                  hintText: "Search",
                                  border: OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Color(0xFFE94444)),
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(10)))),
                            )),
                        ElevatedButton(
                            onPressed: () {
                              debugPrint("Add");
                            },
                            style: ElevatedButton.styleFrom(
                                primary: Color(0xFFE94444),
                                onPrimary: Colors.white,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(50))),
                            child: Icon(Icons.add))
                      ]),
                  SizedBox(
                    height: 12,
                  ),
                  Expanded(
                      child: ListView.builder(
                    itemExtent: 72,
                    itemCount: 11,
                    itemBuilder: (context, index) {
                      return ListTile(
                        leading: Icon(Icons.account_circle),
                        title: Text('User $index'),
                      );
                    },
                  )),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width * 0.75,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 12,
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.05,
                    decoration: BoxDecoration(
                        border: Border(
                      bottom: BorderSide(
                        color: Color(0xFFE94444),
                        width: 3.0,
                      ),
                    )),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        ElevatedButton(
                            onPressed: () {
                              debugPrint("Disable");
                            },
                            style: ElevatedButton.styleFrom(
                                primary: Color(0xFFE94444),
                                onPrimary: Colors.white,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(50))),
                            child: Text("Disable")),
                        ElevatedButton(
                            onPressed: () {
                              debugPrint("Edit");
                            },
                            style: ElevatedButton.styleFrom(
                                primary: Color(0xFFE94444),
                                onPrimary: Colors.white,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(50))),
                            child: Text("Edit")),
                        ElevatedButton(
                            onPressed: () {
                              debugPrint("Delete");
                            },
                            style: ElevatedButton.styleFrom(
                                primary: Color(0xFFE94444),
                                onPrimary: Colors.white,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(50))),
                            child: Text("Delete")),
                      ],
                    ),
                  ),
                ],
              ),
            )
          ],
        ));
  }
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Color(0xFFE94444),
        accentColor: Colors.white,
        scaffoldBackgroundColor: Colors.white,
      ),
      home: DefaultTabController(
        length: 4,
        child: Scaffold(
          appBar: AppBar(
            title: Text("O'Ptit Dej - Admin Web Console"),
            bottom: TabBar(
              tabs: [
                Text(
                  "Users",
                  style: TextStyle(fontSize: 22),
                ),
                Text("Groups", style: TextStyle(fontSize: 22)),
                Text("Roles", style: TextStyle(fontSize: 22)),
                Text("Rights", style: TextStyle(fontSize: 22)),
              ],
            ),
            actions: [
              IconButton(
                  onPressed: () {
                    debugPrint("Logout");
                  },
                  icon: Icon(Icons.power_settings_new))
            ],
          ),
          body: TabBarView(
            children: [
              Users(),
              Text("Groups"),
              Text("Roles"),
              Text("Rights"),
            ],
          ),
        ),
      ),
    );
  }
}
